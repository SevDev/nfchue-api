const fs = require("fs")
const express = require("express")
const HueApi = require("philips-hue")

const config = require("./config")

const app = express()
const hue = new HueApi()
const bridgeCfg = process.cwd().replace(/\\/g, "/") + "/" + config.bridgeFile

app.get("/api", (req, res) => {
	if (req.query.pass == config.passphrase) {
		hue.login(bridgeCfg)
		.then((conf) => {
			hue.getLights().then((lights) => {
				let idLights = Object.keys(lights)

				for (i = 1; i <= idLights.length; i++) {
					let light = hue.light(i)
					let lightId = i
		
					light.getInfo().then((info) => {
						if (info["name"] == config.lampName) {
							hue.light(lightId).on()
							res.status(200).end()
						}
					})
				}
			})
			return true
		})
		.catch((err) => {
			console.error(err.stack || err)
			res.status(404).end()
		})
	} else {
		res.end("Wrong passphrase")
	}
})

app.listen(config.port, () => {
	console.log(`Server running on port ${config.port}`)
})