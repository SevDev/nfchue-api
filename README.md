# NFC Hue API

Web API to control Hue lights

## Installation
* npm i
* node ./index.js

## Requirements
* NodeJS
* npm

## Techs
* [ExpressJS](http://expressjs.com/)
* [Philips Hue](https://github.com/shokai/node-philips-hue/tree/master/packages/philips-hue#readme)
